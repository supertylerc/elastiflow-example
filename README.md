# netflow-analytics

## Getting Started

> If you're running this on Docker for Windows or Docker for Mac, you
> may need to increase the RAM for that VM.  I needed 4G.  If running
> on Linux, you may need to set a `mem_limit` to prevent it from consuming
> too much RAM.

You need `docker-compose`.  Get it with: `pip install docker docker-compose`.
It's recommended that you be in a virtual environment.

Next, bring the base environment up: `docker-compuse up -d`.  Note that this
can take a while, so you should walk away for 10-15 minutes (depending on if
you've already built the container or not).  If you've previously built the
container image, 5-10 minutes should be sufficient.  So grab some coffee!

## Setup Kibana

Log in to Kibana at `http://127.0.0.1:5601`.  If you get a `Connection Reset`
error, try again in 2-3 minutes.  Raise an issue if you can't access Kibana
after 20 minutes.

Next, set the index to `netflow-*`.  This requires that the other containers
are running.

Set the `Time Filter field name` to `netflow.last_switched`.

Import the dashboard from this repository.  First, download it.  Next, import
it in Kibana by navigating to: `Management > Saved Objects > Import`.  Select
the JSON file.  It will ask you about renaming indexes; just say yes.

Once this is finished, go to `Dashboard` and look at the dashboards!

## Machine Learning

Stream data to ELK non-stop for two hours.  This will let the ML features form
a baseline.  Next, you can stream a burst of traffic with something like this:

```
docker-compose scale spike=15
docker-compose scale nflow=10
```

Let this go for a while, and then start creating your first ML reports!  Here's
one to get you started.

Navigate to `Machine Learning > Create new job > Create a single metric job`.
Next, select the `netflow-*` index.  Select `Sum` from the `Aggregation`
drop-down.  From the `Fields` drop-down, select `netflow.bytes`.  Set the
`Bucket span` to `5m`.  Set the `Name` to something like `sum-of-bytes`.  Click
the `Create job` button, and then click the `Continue job in real-time` checkbox.
Finally, click `Apply`.

Enjoy exploring!

## Credit

- [Rob Cowart](https://github.com/robcowart), maintainer of
  [ElastiFlow](https://github.com/robcowart/elastiflow).
- [Sébastien Pujadas](https://github.com/spujadas), maintainer of the
  [ELK+X-Pack all-in-one Docker container](https://github.com/spujadas/elkx-docker).
- [Brent Salisbury](https://github.com/nerdalert), maintainer of the
  [Netflow Generator Docker container](https://github.com/nerdalert/nflow-generator).
